var Hose = require('flowr-hose');
var pug = require('pug');

var hose = new Hose({
  name: 'pug',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("flowr-pug is running...");

hose.process('render', process.env.CONCURRENCY || 1, function(job, done){
	// check for needed params!!
	job.log('render: starting transform operation');
	render(job.data, function(err, output){
		job.log('render: operation finished');
    job.log(JSON.stringify(output));
		done(err, output);
	});
});

function render(data, cb){
  var template = data.template
  var input = data.input

  var html = pug.render(template, {input: input});

  cb(null, html)

}
